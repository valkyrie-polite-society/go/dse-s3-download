package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/valkyrie-polite-society/go/dse-s3-download/pkg"
)

// checkCollisionCmd represents the checkCollision command
var checkCollisionCmd = &cobra.Command{
	Use:   "check-collision",
	Short: "Check backup.json for file name collisions within keyspace-table combinations",
	Long:  `Check backup.json for file name collisions within keyspace-table combinations`,
	Run:   checkBackupJsonForFileNameCollisions,
}

func init() {
	rootCmd.AddCommand(checkCollisionCmd)
}

func checkBackupJsonForFileNameCollisions(cmd *cobra.Command, args []string) {
	backupFile, _ := cmd.Flags().GetString("backup-file")

	err := checkSsTables(backupFile)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Validated sstables files do not have name collisions within keyspace table folders.")
}

func checkSsTables(backupFile string) error {
	backup, err := pkg.LoadBackupList(backupFile)
	if err != nil {
		return err
	}

	return backup.CheckSsTables()
}

/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/valkyrie-polite-society/go/dse-s3-download/pkg"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "dse-s3-download",
	Short: "Downloads DSE snapshot files from Amazon S3",
	Long: `Downloads DSE snapshot files from Amazon S3
to the local node.`,
	Run: downloadFiles,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.Flags().String("local-path", "", "Local directory to where sstable files are downloaded")
	rootCmd.MarkFlagRequired("local-path")

	rootCmd.Flags().String("bucket-name", "", "AWS S3 bucket name where the snapshots are located")
	rootCmd.MarkFlagRequired("bucket-name")

	rootCmd.Flags().String("sstable-prefix", "", "AWS S3 bucket prefix where individual sstable files are located")
	rootCmd.MarkFlagRequired("sstable-prefix")

	rootCmd.Flags().String("keyspaces", "", "A comma-separated list of keyspaces")
	rootCmd.MarkFlagRequired("keyspaces")

	rootCmd.Flags().Int("concurrency", 8, "Sets the maximum concurrency value for the AWS S3 download manager (will download multiple chunks concurrently)")

	rootCmd.PersistentFlags().String("backup-file", "backup.json", "Local path to backup.json file")
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.dse-s3-download.yaml)")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".dse-s3-download" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".dse-s3-download")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func downloadFiles(cmd *cobra.Command, args []string) {
	backupFile, _ := cmd.PersistentFlags().GetString("backup-file")
	localPath, _ := cmd.Flags().GetString("local-path")
	bucketName, _ := cmd.Flags().GetString("bucket-name")
	sstablePrefix, _ := cmd.Flags().GetString("sstable-prefix")
	keyspacesStr, _ := cmd.Flags().GetString("keyspaces")
	concurrency, _ := cmd.Flags().GetInt("concurrency")

	keyspaces := strings.Split(keyspacesStr, ",")
	config := pkg.NewConfig(localPath, bucketName, sstablePrefix, concurrency)

	backup, err := pkg.LoadBackupList(backupFile)
	if err != nil {
		log.Fatal(err)
	}

	dirs, err := pkg.GetCassandraDataDirectories(localPath)
	if err != nil {
		log.Fatal(err)
	}

	sstables := pkg.EntriesForKeyspaces(backup.SsTables, keyspaces)

	// TODO: Parallelize this piece
	for i, sstable := range sstables {
		fmt.Printf("Downloading %d of %d files\n", i+1, len(sstables))
		err := config.DownloadSsTable(sstable, dirs)
		if err != nil {
			log.Fatal(err)
		}
	}
}

package main

import (
	"gitlab.com/valkyrie-polite-society/go/dse-s3-download/cmd"
)

func main() {
	cmd.Execute()
}

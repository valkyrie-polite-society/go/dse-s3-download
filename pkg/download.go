package pkg

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
)

func (c *Config) DownloadSsTable(entry SsTable, dataDirectories map[string]string) error {
	fmt.Printf("Keyspace: %s Table: %s, File: %s (%d)\n", entry.Keyspace, entry.Table, entry.FileName, entry.Size)

	// Look up data directory in map
	tableDirectory, exists := dataDirectories[fmt.Sprintf("%s,%s", entry.Keyspace, entry.Table)]
	if !exists {
		return fmt.Errorf("data directory not found for keyspace %s, table %s", entry.Keyspace, entry.Table)
	}

	// Create the sstable file within the data directory.
	localFile := filepath.Join(tableDirectory, localSsTableFileName(entry))
	f, err := os.Create(localFile)
	if err != nil {
		return err
	}

	// Download it from S3.
	n, err := c.Downloader.Download(f, &s3.GetObjectInput{
		Bucket: aws.String(c.BucketName),
		Key:    aws.String(ssTableKeyPath(c.SsTablePrefix, entry.FileName)),
	})
	if err != nil {
		return err
	}
	fmt.Printf("file downloaded, %d bytes\n", n)

	return nil
}

func ssTableKeyPath(prefix, fileName string) string {
	return filepath.Join(prefix, fileName)
}

func (c *Config) DownloadSsTableFake(entry SsTable) error {
	fmt.Printf("%+v\n", entry)
	return nil
}

func localSsTableFileName(entry SsTable) string {
	return fmt.Sprintf("%s-%d-%s%s.db",
		entry.Version,
		entry.Generation,
		entry.Format,
		entry.Type)
}

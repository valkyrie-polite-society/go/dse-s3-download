package pkg

type Topology struct {
	Datacenter string   `json:"dc"`
	HostID     string   `json:"host_id"`
	IP         string   `json:"ip"`
	Tokens     []string `json:"tokens"`
}

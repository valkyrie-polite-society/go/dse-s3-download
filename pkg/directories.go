package pkg

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

/* Enumerates all directories from the base path and finds the actual table directory.
   Builds a map where the keys are "keyspace_name,table_name" and the value is the table directory for Cassandra.
   For example, if called with "/tmp/walk" and the filesystem structure is

		find /tmp/walk -type d
		/tmp/walk
		/tmp/walk/k3
		/tmp/walk/k3/t2-48732
		/tmp/walk/k3/t1-736
		/tmp/walk/k4
		/tmp/walk/k4/t2-38736
		/tmp/walk/k4/t2-38736/snapshot
		/tmp/walk/k4/t2-38736/backup
		/tmp/walk/k2
		/tmp/walk/k2/t1-38736
		/tmp/walk/k2/t1-38736/snapshot
		/tmp/walk/k2/t1-38736/snapshots
		/tmp/walk/k2/t1-38736/backup
		/tmp/walk/k1
		/tmp/walk/k1/t2
		/tmp/walk/k1/t1

   it should return the following map and no error:

   map[
	 k2,t1:/tmp/walk/k2/t1-38736
	 k3,t1:/tmp/walk/k3/t1-736
	 k3,t2:/tmp/walk/k3/t2-48732
	 k4,t2:/tmp/walk/k4/t2-38736
   ]
*/
func GetCassandraDataDirectories(basePath string) (map[string]string, error) {
	directories := make(map[string]string)

	err := filepath.Walk(basePath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if !info.IsDir() {
				return nil
			}

			dataDirPath := strings.TrimPrefix(path, basePath)
			if len(dataDirPath) > 0 {
				if strings.Count(dataDirPath, "/") == 2 {
					dataDirPath = strings.TrimPrefix(dataDirPath, "/")
					pathParts := strings.Split(dataDirPath, "/")

					keyspace := pathParts[0]

					if strings.Count(pathParts[1], "-") == 1 {
						tableDirParts := strings.Split(pathParts[1], "-")

						table := tableDirParts[0]

						directories[fmt.Sprintf("%s,%s", keyspace, table)] = path
					}
				}
			}

			return nil
		})
	if err != nil {
		return nil, err
	}

	return directories, nil
}

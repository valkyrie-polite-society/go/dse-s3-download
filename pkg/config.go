package pkg

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

type Config struct {
	LocalPath     string
	BucketName    string
	SsTablePrefix string
	Downloader    *s3manager.Downloader
	MaxWorkers    int
	MaxQueueSize  int
}

func NewConfig(localPath string, bucketName string, ssTablePrefix string, concurrency int) *Config {
	sess := session.Must(session.NewSession())
	downloader := s3manager.NewDownloader(sess)
	downloader.Concurrency = concurrency

	return &Config{
		LocalPath:     localPath,
		BucketName:    bucketName,
		SsTablePrefix: ssTablePrefix,
		Downloader:    downloader,
		MaxWorkers:    8,
		MaxQueueSize:  24,
	}
}

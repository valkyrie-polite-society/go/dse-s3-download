package pkg

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_localSsTableFileName(t *testing.T) {
	expected := "md-7285-big-Data.db"

	actual := localSsTableFileName(SsTable{
		Version:    "md",
		Generation: 7285,
		Format:     "big-",
		Type:       "Data",
	})

	assert.Equal(t, expected, actual)
}

func Test_ssTableKeyPath(t *testing.T) {
	expected := "scheduled-backups/snapshots/host_id/sstables/somepath.db"
	actual := ssTableKeyPath("scheduled-backups/snapshots/host_id/sstables", "somepath.db")
	assert.Equal(t, expected, actual)
}

package pkg

import "fmt"

type SsTable struct {
	FileName   string `json:"name"`
	Keyspace   string `json:"keyspace"`
	Table      string `json:"cf"`
	Format     string `json:"format"`
	Generation int    `json:"generation,string"`
	Size       int    `json:"size"`
	Type       string `json:"type"`
	Uniquifier string `json:"uniquifier"`
	Version    string `json:"version"`
}

//EntriesForKeyspaces returns a subset of sstables with the entries corresponding to the desired keyspaces
func EntriesForKeyspaces(sstables []SsTable, keyspaces []string) []SsTable {
	result := make([]SsTable, 0)

	for _, entry := range sstables {
		if StringArrayContains(keyspaces, entry.Keyspace) {
			result = append(result, entry)
		}
	}

	return result
}

func checkSsTables(sstables []SsTable) error {
	collisionMap := make(map[string][]string)

	for _, sstable := range sstables {
		key := fmt.Sprintf("%s,%s", sstable.Keyspace, sstable.Table)
		fileName := localSsTableFileName(sstable)
		files, present := collisionMap[key]
		if present {
			if StringArrayContains(files, fileName) {
				return ssTableCollisionError(key, fileName, sstable)
			} else {
				files = append(files, fileName)
				collisionMap[key] = files
			}
		} else {
			files = make([]string, 1)
			files = append(files, fileName)
			collisionMap[key] = files
		}
	}

	return nil
}

func ssTableCollisionError(key, fileName string, sstable SsTable) error {
	return fmt.Errorf("%s already contains %s (%+v)", key, fileName, sstable)
}

func StringArrayContains(arr []string, value string) bool {
	for _, s := range arr {
		if s == value {
			return true
		}
	}

	return false
}

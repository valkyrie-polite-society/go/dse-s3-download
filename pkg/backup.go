package pkg

import (
	"encoding/json"
	"io/ioutil"
)

//"topology",     # list of structure containing tokens, data center, host id, ip (this is a possible validation point that tokens match)

type Backup struct {
	Name        string              `json:"name"`
	Date        string              `json:"date"`
	Version     int                 `json:"version"`
	Compressed  bool                `json:"compressed"`
	Keyspaces   map[string][]string `json:"keyspaces"`
	Datacenters []string            `json:"datacenters"`
	SsTables    []SsTable           `json:"sstables"`
	Topology    []Topology          `json:"topology"`
}

func LoadBackupList(path string) (*Backup, error) {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	backup := &Backup{}

	err = json.Unmarshal(content, &backup)
	if err != nil {
		return nil, err
	}

	return backup, nil
}

func (backup *Backup) CheckSsTables() error {
	return checkSsTables(backup.SsTables)
}

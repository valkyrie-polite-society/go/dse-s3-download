package pkg

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_checkSsTables(t *testing.T) {
	tests := []struct {
		Tables        []SsTable
		ExpectedError error
	}{
		{
			Tables: []SsTable{
				{
					Keyspace:   "k1",
					Table:      "t1",
					Version:    "md",
					Generation: 2,
					Format:     "big-",
					Type:       "Data",
				},
				{
					Keyspace:   "k1",
					Table:      "t1",
					Version:    "md",
					Generation: 3,
					Format:     "big-",
					Type:       "Data",
				},
			},
			ExpectedError: nil,
		},
		{
			Tables: []SsTable{
				{
					Keyspace:   "k1",
					Table:      "t1",
					Version:    "md",
					Generation: 2,
					Format:     "big-",
					Type:       "Data",
				},
				{
					Keyspace:   "k1",
					Table:      "t1",
					Version:    "md",
					Generation: 3,
					Format:     "big-",
					Type:       "Data",
				},
				{
					Keyspace:   "k1",
					Table:      "t2",
					Version:    "md",
					Generation: 3,
					Format:     "big-",
					Type:       "Data",
				},
			},
			ExpectedError: nil,
		},
		{
			Tables: []SsTable{
				{
					Keyspace:   "k1",
					Table:      "t1",
					Version:    "md",
					Generation: 2,
					Format:     "big-",
					Type:       "Data",
				},
				{
					Keyspace:   "k1",
					Table:      "t1",
					Version:    "md",
					Generation: 3,
					Format:     "big-",
					Type:       "Data",
				},
				{
					Keyspace:   "k1",
					Table:      "t2",
					Uniquifier: "4567",
					Version:    "md",
					Generation: 5,
					Format:     "big-",
					Type:       "Data",
				},
				{
					Keyspace:   "k1",
					Table:      "t2",
					Uniquifier: "8762",
					Version:    "md",
					Generation: 5,
					Format:     "big-",
					Type:       "Data",
				},
			},
			ExpectedError: ssTableCollisionError("k1,t2", "md-5-big-Data.db", SsTable{
				Keyspace:   "k1",
				Table:      "t2",
				Uniquifier: "8762",
				Version:    "md",
				Generation: 5,
				Format:     "big-",
				Type:       "Data",
			}),
		},
	}

	for _, test := range tests {
		err := checkSsTables(test.Tables)

		if test.ExpectedError != nil {
			assert.EqualError(t, err, test.ExpectedError.Error())
		} else {
			assert.NoError(t, err)
		}
	}
}

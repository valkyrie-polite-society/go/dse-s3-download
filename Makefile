linux:
	env GOOS=linux GOARCH=amd64 go build -o bin/dse-s3-download

darwin:
	env GOOS=darwin GOARCH=amd64 go build -o bin/dse-s3-download.darwin

clean:
	rm -rf bin/*

module gitlab.com/valkyrie-polite-society/go/dse-s3-download

go 1.16

require (
	github.com/aws/aws-sdk-go v1.38.51 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/spf13/cobra v1.1.3 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
)
